class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary
    }

    set name(value) {
        if (value.length >= 3) {
            this._name = value;
        }

        else {
            console.error('empty name field');
            return
        }
    }

    set age(value) {
        if (value > 0) {
            this._age = value;
        }

        else {
            console.error('age cannot be < 0');
            return
        }
    }


    set salary(value) {
        if (value > 0) {
            this._salary = value;
        }

        else {
            console.error('salary cannot be < 0');
            return
        }
    }


    get name() {
        return this._name
    }

    get age() {
        return this._age
    }

    get salary() {
        return this._salary
    }
}


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    set salary(value) {
        if (value > 0) {
            this._salary = value * 3;
        }

        else {
            console.error('salary cannot be < 0');
            return
        }
    }

    get salary() {
        return this._salary
    }

};

let user1 = new Programmer('Sofia', 22, 5000, 'deutch');
console.log(user1);

let user2 = new Programmer('Rostik', 33, 7000, ['java', 'python']);
console.log(user2)